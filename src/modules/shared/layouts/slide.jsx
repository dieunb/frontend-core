import React, {Component} from 'react';
import '../../../App.css';
import '../../../bootstrap_business.css'
import 'bootstrap/less/bootstrap.less'
import 'jquery/src/jquery.js'
import 'bootstrap/dist/js/bootstrap.js';
class Slide extends Component {
  render() {
    return (
      <div>
        <div className="slider">
          <a href="http://fb.com/datnguyen2104"className="arrow_top">
            <img src="http://gogi.com.vn/Content/images/arrow_top.png" alt=""/></a>
          <div id="carousel-example-generic" className="carousel slide" data-ride="carousel">

            <ol className="carousel-indicators">
              <li data-target="#carousel-example-generic" data-slide-to="0" className=""></li>
              <li data-target="#carousel-example-generic" data-slide-to="1" className=""></li>
            </ol>

            <div className="carousel-inner" role="listbox">

              <div className="item">
                <img src="http://gogi.com.vn/media/4006/slideweb2.jpg" className="img-responsive" alt="Home banner 4"/>
                <a href="http://gogi.com.vn/vi/blog/nhu-the-nao-la-thit-nuong-chuan-han/">
                  <div className="carousel-caption">
                    <p>GoGi House - Quán Thịt Nướng Hàn Quốc
                    </p>
                  </div>
                </a>
              </div>
              <div className="item">
                <img src="http://gogi.com.vn/media/4031/toanban-gogi.jpg" className="img-responsive" alt="ảnh toàn bàn"/>
                <a href="http://gogi.com.vn/vi/%C6%B0u-dai/ha-noi/xeo-xeo-gogi-chuan-vi-han-quoc/">
                  <div className="carousel-caption">
                    <p>GoGi House - Quán Thịt Nướng Hàn Quốc
                    </p>
                  </div>
                </a>
              </div>
              <div className="item">
                <img src="http://gogi.com.vn/media/4009/slideweb5.jpg" className="img-responsive" alt="Home banner 6"/>
                <a href="http://gogi.com.vn/vi/blog/nhu-the-nao-la-thit-nuong-chuan-han/">
                  <div className="carousel-caption">
                    <p>GoGi House - Quán Thịt Nướng Hàn Quốc
                    </p>
                  </div>
                </a>
              </div>
              <div className="item active">
                <img src="http://gogi.com.vn/media/4010/slideweb6.jpg" className="img-responsive" alt="Home banner 5"/>
                <a href="http://gogi.com.vn/vi/blog/nhu-the-nao-la-thit-nuong-chuan-han/">
                  <div className="carousel-caption">
                    <p>GoGi House - Quán Thịt Nướng Hàn Quốc
                    </p>
                  </div>
                </a>
              </div>
              <div className="item">
                <img src="http://gogi.com.vn/media/4008/slideweb4.jpg" className="img-responsive" alt="Home banner 1"/>
                <a href="http://gogi.com.vn/vi/blog/nhu-the-nao-la-thit-nuong-chuan-han/">
                  <div className="carousel-caption">
                    <p>GoGi House - Quán Thịt Nướng Hàn Quốc
                    </p>
                  </div>
                </a>
              </div>
              <div className="item">
                <img src="http://gogi.com.vn/media/4005/slideweb1.jpg" className="img-responsive" alt="Home banner 3"/>
                <a href="http://gogi.com.vn/vi/blog/giai-ma-3-vi-lau-ngon-chuan-han-tai-gogi-house/">
                  <div className="carousel-caption">
                    <p>GoGi House - Quán Thịt Nướng Hàn Quốc
                    </p>
                  </div>
                </a>
              </div>
              <div className="item">
                <img src="http://gogi.com.vn/media/2424/hin_7257.png" className="img-responsive" alt="Home banner 7"/>
                <a href="http://gogi.com.vn/vi/%C6%B0u-dai/ha-noi/xeo-xeo-gogi-chuan-vi-han-quoc/">
                  <div className="carousel-caption">
                    <p>GoGi House - Quán Thịt Nướng Hàn Quốc
                    </p>
                  </div>
                </a>
              </div>
            </div>
            <a className="" href="#carousel-example-generic" role="button" data-slide="prev">
              <img src="http://gogi.com.vn/Content/images/arrow_l.png" alt="" className="l_arrow"/></a>
            <a className="" href="#carousel-example-generic" role="button" data-slide="next">
              <img src="http://gogi.com.vn/Content/images/arrow_r.png" alt="" className="r_arrow"/></a>
          </div>
        </div>
      </div>
    )
  }
};
export default Slide;
