import React, {Component} from 'react';
import '../../../App.css';
import '../../../bootstrap_business.css'
import 'bootstrap/less/bootstrap.less'
class Post extends Component {
  render() {
    return (
      <div>
        <div className="about_home">
          <div className="">

            <div className="table_about">
              <div className="img_left_about">
                <img src="http://gogi.com.vn/media/5441/slide-web-732x484px-1.jpg" alt="" className="img-responsive"/></div>
              <div className="txt_right_about">
                <h3>Tiệc Nướng Linh Đình - Xèo Xèo Thỏa Thích</h3>
                <span>Buffet Xèo Xèo với hơn 20 món thịt nướng hảo hạng cùng cực nhiều món ngon chuẩn vị Hàn đổ bộ toàn quốc mang theo cơn lốc Ăn 4 tặng 1 cực khủng không thể bỏ lỡ đó nha!</span>
                <a className="btn_txt_r" href="http://gogi.com.vn/vi/%C6%B0u-dai/ha-noi/tiec-nuong-linh-%C4%91inh-xeo-xeo-thoa-thich/" style={{
                    color: '#808080',
                    fontWeight: 'bold'
                  }}>KHÁM PHÁ NGAY</a>
              </div>
              <div className="clearfix"></div>
            </div>
            <div className="box_mid_promotion">
              <div className="">
                <div className="img_mid_about">
                  <img src="http://gogi.com.vn/media/4683/c_users_diep-mai_appdata_local_packages_microsoft-skypeapp_kzf8qxf38zg5c_localstate_563a13c1-4176-4c2a-9184-cb071984cbe6.png" alt="" className="img-responsive"/></div>
                <div className="txt_mid_about">
                  <h3>Vạn Phúc Hội Tụ - No Tròn Cảm Xúc</h3>
                  <span>Các bạn đã sẵn sàng tới GoGi Vạn Phúc tưng bừng những bữa tiệc nướng no say cùng nhiều món quà cực hấp dẫn nhân dịp khai trương chưa nào ?</span>
                  <a className="btn_mid_r btn_detail_home" href="http://gogi.com.vn/vi/%C6%B0u-dai/ha-noi/van-phuc-hoi-tu-no-tron-cam-xuc/" style={{
                      color: '#808080',
                      fontWeight: 'bold!important'
                    }}>NHẬN QUÀ NGAY</a>
                </div>
                <div className="clearfix"></div>
              </div>
            </div>
            <div className="table_about">
              <div className="img_left_about">

                <img src="http://gogi.com.vn/media/5211/t12-standee-free-beer-pepsi-3_banner-u-u-da-i.png" alt="" className="img-responsive"/></div>
              <div className="txt_right_about">
                <h3>Tặng Coca/ Bia Tươi Thỏa Thích</h3>
                <span>Tới GoGi Xèo Xèo tiệc nướng và đón trọn ưu đãi Tặng Coca/ Bia tươi không giới hạn ngay đi thôi</span>
                <a className="btn_txt_r" href="http://gogi.com.vn/vi/%C6%B0u-dai/ha-noi/tang-coca-bia-tuoi-thoa-thich/" style={{
                    color: '#808080',
                    fontWeight: 'bold!important'
                  }}>NHẬN ƯU ĐÃI
                </a>
              </div>
              <div className="clearfix"></div>
            </div>
          </div>
        </div>
        <div className="book">
          <div class="">
            <div class="right_banner">
              <img src="http://gogi.com.vn/media/3571/sdm_0797.jpg" class="img-responsive" alt="SDM_0797.jpg"/></div>
            <div class="left_book" style={{
                background: '#000'
              }}>
              <div class="txt_book">
                <h3 style={{
                    color: 'white'
                  }}>Như thế nào là thịt nướng chuẩn Hàn ?</h3>
                <p style={{
                    color: 'white'
                  }}>
                  Khi nói đến Hàn Quốc, ẩm thực là nét văn hóa đặc trưng không thể bỏ qua và thịt nướng Hàn Quốc luôn được “truyền tai” về độ tươi ngon, đậm đà qua những trang cẩm nang du lịch hay những bộ phim Hàn gây bão. Cùng GoGi House tìm hiểu lí do nào khiến thịt nướng Hàn Quốc có thể “vi hành” đến khắp nơi trên thế giới như vậy nhé.
                </p>
                <a href="http://gogi.com.vn/vi/blog/nhu-the-nao-la-thit-nuong-chuan-han/" class="btn_txt_r" style={{
                    color: '#808080',
                    fontWeight: 'bold'
                  }}>
                  CHI TIẾT</a>
              </div>
              <div class="clearfix"></div>
              <div class="img_bot">
                <img src="http://gogi.com.vn/media/3572/sdm_0677.jpg" class="img-responsive" alt="SDM_0677.jpg"/></div>

            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    )
  }
}

export default Post;
