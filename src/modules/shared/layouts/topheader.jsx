import React, {Component} from 'react';
import '../../../topheader.css'
class Topheader extends Component {
  render() {
    return(
      <div className="top-header visible-lg visible-md">
        <div className="container">
          <div className="postwcode-widget">
            <div data-vc-full-width="true" data-vc-full-width-init="true" className="vc_row wpb_row vc_row-fluid vc_custom_1528184610111 vc_row-has-fill" style={{
                position: 'relative',
                left: '-48px',
                boxSizing: 'border-box',
                width: '1296px',
                paddingLeft: '48px',
                paddingRight: '48px'
              }}>
              <div className="inline-inner-block wpb_column vc_column_container vc_col-sm-6">
                <div className="vc_column-inner vc_custom_1519641836434">
                  <div className="wpb_wrapper">
                    <div className="wpb_raw_code wpb_content_element wpb_raw_html">
                      <div className="wpb_wrapper">
                        <div className="settings">
                          <div className="inner">

                            <div className="language">
                              <span className="tongle">English</span>
                              <ul>
                                <li>
                                  <a className="en" href="#">English</a>
                                </li>
                                <li>
                                  <a className="it" href="#">Italia</a>
                                </li>
                                <li>
                                  <a className="fr" href="#">French</a>
                                </li>
                              </ul>
                            </div>
                            <div className="currency">
                              <span className="tongle">$ - USD</span>
                              <ul>
                                <li>
                                  <a href="#">$ - USD</a>
                                </li>
                                <li>
                                  <a href="#">€ - EUR</a>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="inline-inner-block float-right wpb_column vc_column_container vc_col-sm-6">
                <div className="vc_column-inner vc_custom_1519641840721">
                  <div className="wpb_wrapper">
                    <div className="sns-info-inline">
                      <a href="tel:012-323-584-98" target="_self">
                        <span style={{fontSize:'16px'}} className="vc_icon_element-icon fa fa-phone"></span>012-323-584-98</a>
                    </div>
                    <div className="sns-info-inline">
                      <a href="#" target="_self">
                        <span style={{fontSize:'16px'}} className="vc_icon_element-icon fa fa-map-marker"></span>Store location</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="vc_row-full-width vc_clearfix"></div>
          </div>
        </div>
      </div>
    )
  }
}

export default Topheader;
