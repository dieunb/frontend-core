import React, {Component} from 'react';
import '../../../App.css';
import '../../../bootstrap_business.css'
import 'bootstrap/less/bootstrap.less'
class Footer extends Component {
  render() {
    return (
      <div>
        <footer>
          <div className="link_footer">
            <div className="container">
              <div className="box_link">
                <hr></hr>
                <h3>VỀ GOLDEN GATE</h3>
                <ul>
                  <li>
                    <a href="http://fb.com/datnguyen2104">TỔNG QUAN</a>
                  </li>
                  <li>
                    <a href="http://fb.com/datnguyen2104">SỨ MỆNH</a>
                  </li>
                  <li>
                    <a href="http://fb.com/datnguyen2104">TĂNG TRƯỞNG</a>
                  </li>
                </ul>
              </div>
              <div className="box_link">
                <hr></hr>
                <h3>THE GOLDEN SPOON</h3>
                <ul>
                  <li>
                    <a href="http://fb.com/datnguyen2104">Hiện đã có trên AppStore và CHPlay</a>
                  </li>
                </ul>
              </div>
              <div className="box_link">
                <hr></hr>
                <h3>VĂN HÓA</h3>
                <ul></ul>
              </div>
              <div className="box_link">
                <hr></hr>
                <h3>
                  TIN TỨC</h3>
                <ul></ul>
              </div>
              <div className="box_link">
                <hr></hr>
                <h3>TUYỂN DỤNG</h3>
                <ul>
                  <li>
                    <a href="http://fb.com/datnguyen2104">Hotline: 19006622</a>
                  </li>
                </ul>
              </div>
              <div className="box_link">
                <hr></hr>
                <h3>LIÊN HỆ</h3>
                <ul></ul>
              </div>
            </div>
          </div>
          <div className="address_footer">
            <div className="container">
              <div className="row">
                <div className="col-md-8 col-sm-6">
                  <div className="address">
                    Công ty Cổ phần Thương mại Dịch vụ Cổng Vàng<br></br>
                    Trụ sở chính: Tầng 6, tòa nhà Toyota, 315 Trường Chinh, Thanh Xuân, Hà Nội<br></br>
                    GPĐK: Số 0103023679 cấp ngày 09/04/2008<br></br>
                    ĐT: 043 222 3000 Email: support.hn@ggg.com.vn
                  </div>
                </div>
                <div className="col-md-4 col-sm-6">
                  <div className="bct">
                    <img src="http://gogi.com.vn/Content/images/bct.png" alt=""/>
                    <p>© 2011 Golden Gate ., JSC. All rights reserved</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </div>
    )
  }
}

export default Footer;
