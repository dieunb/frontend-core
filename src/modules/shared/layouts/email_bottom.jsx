import React, {Component} from 'react';
import '../../../App.css';
import '../../../bootstrap_business.css'
import swal from 'sweetalert'
class EmailBottom extends Component {

  onSubmit = (event) => {
    event.preventDefault();
    const email = document.getElementById('Email');
    if (email) {
      swal({title: "Success !", text: 'Bạn đã đăng ký nhận thông tin từ gogi thành công. Cảm ơn bạn', icon: "success", button: "OK"});
    } else {
      console.log('LOI')
    }
  }

  render() {
    return (<div>
      <div className="email_bottom">
        <div className="container">
          <div className="row">
            <div className="col-md-5">
              <aside id="sticky-social">
                <ul>
                  <li>
                    <a href="https://www.facebook.com/GoGiHouse.QuanThitNuongHanQuoc/" className="entypo-social" target="blank">
                      <img src="http://gogi.com.vn/media/1269/fb-nonhover.png" onMouseOver={e => (e.currentTarget.src = "http://gogi.com.vn/media/1082/fb.png")} onMouseOut={e => (e.currentTarget.src = "http://gogi.com.vn/media/1269/fb-nonhover.png")} alt="fb-nonhover.png"/></a>
                  </li>
                  <li>
                    <a href="https://www.instagram.com/gogihouse.official/" className="entypo-social" target="blank">
                      <img src="http://gogi.com.vn/media/2279/09ffa2be0d6f484e796fb7f3d088a0e8dc19e0c49ee5c16636-pimgpsh_fullsize_distr.png" onMouseOver={e => (e.currentTarget.src = "http://gogi.com.vn/media/1083/instgram.png")} onMouseOut={e => (e.currentTarget.src = "http://gogi.com.vn/media/2279/09ffa2be0d6f484e796fb7f3d088a0e8dc19e0c49ee5c16636-pimgpsh_fullsize_distr.png")} alt="^09FFA2BE0D6F484E796FB7F3D088A0E8DC19E0C49EE5C16636^pimgpsh_fullsize_distr.png"/></a>
                  </li>
                  <li>
                    <a href="https://www.youtube.com/channel/UCd3-QS4vCFnPvAEEpKwgNIw" className="entypo-social" target="blank">
                      <img src="http://gogi.com.vn/media/2278/8f0aef0ca8887089581830ce5416fbdf2fd6644bba8552ca06-pimgpsh_fullsize_distr.png" onMouseOver={e => (e.currentTarget.src = "http://gogi.com.vn/media/1085/youtube.png")} onMouseOut={e => (e.currentTarget.src = "http://gogi.com.vn/media/2278/8f0aef0ca8887089581830ce5416fbdf2fd6644bba8552ca06-pimgpsh_fullsize_distr.png")} alt="^8F0AEF0CA8887089581830CE5416FBDF2FD6644BBA8552CA06^pimgpsh_fullsize_distr.png"/></a>
                  </li>
                </ul>
              </aside>
              <ul className="link-social">
                <li>
                  <a href="https://www.facebook.com/GoGiHouse.QuanThitNuongHanQuoc/">
                    <img src="http://gogi.com.vn/media/1082/fb.png" alt="fb-nonhover.png"/></a>
                </li>
                <li>
                  <a href="https://www.instagram.com/gogihouse.official/">
                    <img src="http://gogi.com.vn/media/1083/instgram.png" alt="^09FFA2BE0D6F484E796FB7F3D088A0E8DC19E0C49EE5C16636^pimgpsh_fullsize_distr.png"/></a>
                </li>
                <li>
                  <a href="https://www.youtube.com/channel/UCd3-QS4vCFnPvAEEpKwgNIw">
                    <img src="http://gogi.com.vn/media/1085/youtube.png" alt="^8F0AEF0CA8887089581830CE5416FBDF2FD6644BBA8552CA06^pimgpsh_fullsize_distr.png"/></a>
                </li>
              </ul>
            </div>
            <div className="col-md-7">

              <form onSubmit={this.onSubmit}>
                <table>
                  <tbody>
                    <tr>
                      <td>
                        <span style={{
                            marginRight: '10px'
                          }}>Đăng ký để nhận ưu đãi hàng tháng</span>
                      </td>
                      <td>
                        <input className="input_email" data-val="true" data-val-length="Email hãy nhập không quá 200 kí tự." data-val-length-max="200" data-val-regex="Email không đúng định dạng." data-val-regex-pattern="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$" data-val-required="Bạn hãy nhập Email." id="Email" name="Email" placeholder="Địa chỉ email" style={{
                            margin: 'auto'
                          }} type="email" value=""/></td>
                      <td>
                        <button className="btn_send" type="submit" style={{
                            marginLeft: '10px'
                          }}>ĐĂNG KÝ</button>
                      </td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>
                        <span style={{
                            color: 'red'
                          }}>
                          <span className="field-validation-valid" data-valmsg-for="Email" data-valmsg-replace="true"></span>
                        </span>
                      </td>
                      <td></td>
                    </tr>
                  </tbody>
                </table>
                <input name="ufprt" type="hidden" value="9E3715BE95429DA0E6AF8FEBBA9463E7BA892F19A4DBE07A1C1C387AF87DAA1DFFFD89CC88EE70038346B9BCE7BF941D5E38D3009D6E8817D147363FE14F24708C4E7EBA2EFA25AB7E08A01D1B3393FC0A5A3EB0724A948AC99DAB09C9C51489864F8F34860943739A1B8C68326EBB6875D99271D84794F7329B9350F4441E278DE03767656AEA971D20D9D6BBB5D26D"/></form>

              <div className="modal fade" id="popupSubcribe" role="dialog">
                <div className="modal-dialog">
                  <div className="modal-content">
                    <div className="modal-header">
                      <button type="button" className="close" data-dismiss="modal">×</button>
                      <h4 className="modal-title">Thông báo</h4>
                    </div>
                    <div className="modal-body">
                      <img src="http://gogi.com.vn/Content/images/attention.png" alt=""/>
                      <h4>THÔNG TIN CẢNH BÁO</h4>
                      <p></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>)
  }
}

export default EmailBottom;
