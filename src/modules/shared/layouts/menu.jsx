import React, {Component} from 'react';
import '../../../App.css';
import '../../../bootstrap_business.css'
import 'bootstrap/less/bootstrap.less'
class Menu extends Component {
  render() {
    return (<div>
      <div className="">
        <ul className="list-inline text-center list-inline-1" style={{
            border: 'none'
          }}>
          <li className="active" style={{
              border: '1px',
              marginRight: '10px'
            }}>
            <a data-toggle="tab" href="#0">THỰC ĐƠN</a>
          </li>
        </ul>
        <div className="tab-content">
          <div id="0" className="tab-pane fade  in active">
            <img src="http://gogi.com.vn/media/1977/bang-gia-01.png" alt="bang gia-01.png" style={{
                width: '100%'
              }}/>
          </div>
        </div>
      </div>
      <div className="box-new">
        <div className="box_mid_promotion">
          <div className="img_mid_about">
            <div className="col-xs-6 col-md-6">
              <img src="http://gogi.com.vn/media/2419/menu_trang_chu_xeo_xeo_gogi.png" alt="" className="img-responsive"/>
              <a className="btn_txt_r" href="http://gogi.com.vn/vi/thuc-don/">BUFFET XÈO XÈO
              </a>
            </div>
            <div className="col-xs-6 col-md-6">
              <img src="http://gogi.com.vn/media/2448/lau2.png" alt="" className="img-responsive"/>
              <a className="btn_txt_r" href="http://gogi.com.vn/vi/thuc-don/">GỌI MÓN</a>
            </div>
          </div>
          <div className="txt_mid_about">
            <h2>Quán Thịt Nướng Hàn Quốc</h2>
            <span>GoGi House (Quán thịt nướng Hàn Quốc) sẽ đưa bạn đến Seoul, nơi những con phố bình dị, những quán ăn đã trở nên quen thuộc và gắn bó với người dân xứ Hàn. Nếu đã một lần thưởng thức thịt nướng tại GoGi House, bạn sẽ không thể quên được hương vị "ngất ngây" của những món sườn non bò Mỹ, nạc vai bò Mỹ, dẻ sườn tươi.... khi hòa quyện vào với các loại gia vị đặc trưng của xứ sở Kimchi đã trở nên hấp dẫn đến thế nào. Ngoài ra, những món ăn kèm không thể bỏ qua như cơm trộn, mỳ lạnh, canh Kimchi và các loại lẩu cũng sẽ làm bạn ấn tượng thêm về nền ẩm thực Hàn Quốc</span>
            <a className="btn_mid_r" style={{
                textDecoration: 'underline',
                fontWeight: 'bold'
              }} href="http://gogi.com.vn/vi/thuc-don/">
              <i>Xem thực đơn</i>
            </a>

          </div>
          <div className="clearfix"></div>
        </div>
      </div>
    </div>)
  }
}

export default Menu;
