import React, {Component} from 'react';
import Slide from '../shared/layouts/slide.jsx'
import Menu from '../shared/layouts/menu.jsx'
import Post from '../shared/layouts/post.jsx'
import EmailBottom from '../shared/layouts/email_bottom.jsx'
import Header from '../shared/layouts/header.jsx';
class Home extends Component {
  render() {
    return(
      <div>
        <Header />
        <Slide />
        <Menu />
        <Post />
        <EmailBottom />
      </div>
    )
  }
}

export default Home;
