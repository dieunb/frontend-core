import React, {Component} from 'react';
import {Link} from 'react-router-dom';
class ShowCategory extends Component {
  render() {
    return (
      <div>
        <header className="header1" id="header_nav">
          <nav className="navbar navbar-default navbar-fixed-top">
            <div className="container">
              <div className="navbar-header">
                <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span className="sr-only">Toggle navigation</span>
                  <span className="icon-bar"></span>
                  <span className="icon-bar"></span>
                  <span className="icon-bar"></span>
                </button>
                <a className="navbar-brand" href="/">
                  <img src="http://gogi.com.vn/media/3069/logo-gogi.png" alt="ahihi"/></a>
              </div>
              <div className="langue">
                <a href="http://gogi.com.vni/">
                  <img src="http://gogi.com.vn/media/1101/vn.png" alt=""/></a>
                <a href="http://gogi.com.vn/voucher/">
                  <img alt=""/></a>
                <a href="http://gogi.com.vn/promotions/">
                  <img alt=""/></a>
                <a href="http://gogi.com.vn/sitemapxml/">
                  <img alt=""/></a>
                <a href="http://gogi.com.vn/robots/">
                  <img alt=""/></a>
              </div>
              <div>
                <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul className="nav navbar-nav navbar-right">
                    <li>
                      <Link to = "/vi/thuc-don/">
                        <img alt=""/>
                        Thực đơn
                      </Link>
                    </li>
                    <li>
                      <Link to="/vi/ưu-dai/">
                        <img alt=""/>
                        Ưu đãi
                      </Link>
                    </li>
                    <li>
                      <Link to="http://thegoldenspoon.ggg.com.vn/?__hstc=15493353.a2e885edb86ce5556f88e18eb145bf9d.1542186063327.1542187985573.1542195804305.3&amp;__hssc=15493353.1.1542195804305&amp;__hsfp=966167841">
                        <img src="http://gogi,com.vn/media/1151/ico_spoon.png" alt=""/>
                        THE GOLDEN SPOON
                      </Link>
                    </li>
                    <li>
                      <Link to="/vi/blog/">
                        <img alt=""/>
                        Blog
                      </Link>
                    </li>
                    <li>
                      <Link to="https://www.facebook.com/TuyendungGoldenGateGroup/?ref=br_rs">
                        <img alt=""/>
                        Tuyển dụng
                      </Link>
                    </li>
                    <li>
                      <Link to="/vi/lien-he/">
                        <img alt=""/>
                        Liên hệ
                      </Link>
                    </li>
                    <li>
                      <Link to="http://gogi.com.vn/vi/đat-ban/">
                        <img alt=""/>
                        Đặt bàn
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </nav>
        </header>
      </div>
    )
  }
}

export default ShowCategory;
