import React from 'react';
import Home from '../modules/Home/Home.js';
import ShowCategory from '../modules/Category/ShowCategory.jsx';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
const Router = () => (
  <BrowserRouter>
    <Switch>
        <Route exact path='/' component={Home} />
        <Route path='/vi/thuc-don' component={ShowCategory} />
        <Route path='/vi/ưu-dai' component={Home} />
        <Route path='/vi/lien-he' component={ShowCategory} />
    </Switch>
  </BrowserRouter>
);

export default Router;
