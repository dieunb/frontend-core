import React, { Component } from 'react';
import Router from './Router';
//import Topheader from './modules/shared/layouts/topheader.jsx';
import Footer from './modules/shared/layouts/footer.jsx';

class App extends Component {
  render() {
    return (
      <div>
        <Router />
        <Footer />
      </div>
    );
  }
}

export default App;
